/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wifs;

import com.wifs.utils.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author vignesh
 */
public class SESDAO {
    public static Logger logger = Logger.getLogger("SESDAO.class");
    
    public ArrayList<SESBO> getEmailList(){
        DBConnection objDBConnection = new DBConnection();
        Connection con	=	null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<SESBO> emailList = new ArrayList<>();
        try {
            String sqlQuery = "select *, u.email from SESTable s join userinfo u on s.InvestorId = u.userid and u.userid in (372303, 372304, 372305, 372306, 372307,386637,372313,372314,372315,380365,372316,388414,372317,372318,390530,389427,372308,372309,372310,372311)";
                            con = objDBConnection.getWifsSQLConnect();
                ps = con.prepareStatement(sqlQuery);
                rs = ps.executeQuery();
                while (rs.next()){
                    SESBO sesObj = new SESBO();
                    sesObj.setEmailid(rs.getString("email"));
                    sesObj.setInvestorName(rs.getString("name"));
                    sesObj.setTransType(rs.getString("TransType"));
                    sesObj.setAccountNumber(rs.getString("AccountNumber"));
                    sesObj.setBankName(rs.getString("BankName"));
                    sesObj.setECS_DR_DATE(rs.getDate("ECS_DR_DATE"));
                    sesObj.setECS_TOT_AMT(rs.getInt("ECS_TOT_AMT"));
                    sesObj.setInvestorAmount(rs.getInt("InvestorAmount"));
                    sesObj.setSchemeName(rs.getString("SchemeName"));
                    emailList.add(sesObj);
                }
        } catch (Exception ex){
            logger.error("Error in getting list from database : "+ex);
        }
        return emailList;
    }
}
