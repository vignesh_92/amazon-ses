/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wifs;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import javax.mail.Session;
import org.apache.log4j.Logger;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.BulkEmailDestination;
import com.amazonaws.services.simpleemail.model.CreateTemplateRequest;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.GetTemplateRequest;
import com.amazonaws.services.simpleemail.model.MessageTag;
import com.amazonaws.services.simpleemail.model.SendBulkTemplatedEmailRequest;
import com.amazonaws.services.simpleemail.model.SendBulkTemplatedEmailResult;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendTemplatedEmailRequest;
import com.amazonaws.services.simpleemail.model.Template;
import java.util.List;
import java.util.concurrent.ExecutorService;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author vignesh
 */
public class SESMail {

    public static Logger logger = Logger.getLogger("SESMail.class");

    public void sendMailSES() throws IOException {

        Properties properties = new Properties();
        Properties sendMailproperties = new Properties();
        // From Properties files
        File fileDir = new File(".");
        String strDirPath = fileDir.getCanonicalPath();
        //logger.debug(" Property File Directory : "+strDirPath);
        FileInputStream PropertyFile = new FileInputStream(strDirPath + File.separator + "properties" + File.separator + "config.properties");
        properties.load(PropertyFile);
        FileInputStream sendMailPropertyFile = new FileInputStream(strDirPath + File.separator + "properties" + File.separator + "SendMailConfig.properties");
        sendMailproperties.load(sendMailPropertyFile);
        // Create a Properties object to contain connection configuration information.
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtps");
        props.put("mail.smtp.port", properties.getProperty("PORT"));

        // Set properties indicating that we want to use STARTTLS to encrypt the connection.
        // The SMTP session will begin on an unencrypted connection, and then the client
        // will issue a STARTTLS command to upgrade to an encrypted connection.
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.starttls.required", "true");

        // Create a Session object to represent a mail session with the specified properties. 
        Session session = Session.getDefaultInstance(props);
        SESDAO sesBOObj = new SESDAO();
        ArrayList<SESBO> emailList = new ArrayList<>();
        BasicAWSCredentials basicCreds = new BasicAWSCredentials("AKIAIQW6BTIU5B6YSTKQ", "/zMquxq0ZRixSPd8cet0wHbcfAH8atlk0JjCZoXi");
        AmazonSimpleEmailService client = null;
        try {
            client = AmazonSimpleEmailServiceClientBuilder.standard().withRegion(Regions.US_WEST_2).withCredentials(new AWSStaticCredentialsProvider(basicCreds)).build();
        } catch (Exception ex) {
            logger.info(ex);
        }
        SendBulkTemplatedEmailRequest sbter = new SendBulkTemplatedEmailRequest();
        List<BulkEmailDestination> bulkDestinations = new ArrayList<>();
        JSONObject defaultReplacementTempData = new JSONObject();
//        CreateTemplateRequest ctr = new CreateTemplateRequest();
//        Template template = new Template();
//        template.setTemplateName("TPSL_Sample_SIP_Template2");
//        template.setSubjectPart("Upcoming investment in your account- Bulk Email Test");
//        template.setHtmlPart("<html><body>"
//                + "<span style='font-size:12px;'>"
//                + "<table border=0 cellspacing=0 cellpadding=0>"
//                + "<tr><td>Hello {{Investor_Name}},</td></tr>"
//                + "<tr><td> <br /> </td></tr>"
//                + "<tr><td> Greetings from {{LabelShortName}}</td></tr>"
//                + "<tr><td> <br /> </td></tr>"
//                + "<tr><td>We are writing to inform you that the following {{TransType}} transaction has been successfully executed.</td></tr>"
//                + "<tr><td> <br /> </td></tr>"
//                + "<tr><td>Details:</td></tr>"
//                + "<tr><td> <br /> </td></tr>"
//                +"<tr><td><table border='1' cellspacing='0' cellpadding='2'><tr><th>Scheme name</th><th>Amount (Rs.)</th><th>Transaction Type</th></tr>"
//                + "<tr><td> {{SchemeName}}</td><td align='right'>{{InvestorAmount}}</td>"
//                + "<td>{{TransType}}</td></tr>"
//                + "</table></td></tr>"
//                + "As an investment date is approaching soon, this mail is to remind you that a debit of Rs. {{ECS_TOT_AMT}} will be made from your bank on {{ECS_DR_DATE}} . "
//                + "We kindly advise you to ensure that your bank account is adequately funded for this debit."
//                + "Please note that the investment will be made as soon as {{LabelShortName}} receives confirmation that the bank has successfully debited your account "
//                + "<tr><td> <br /> </td></tr>"
//                + "<tr><td>Bank:{{BankName}}</td></tr>"
//                + "<tr><td>A/c No.: {{AccountNumber}}</td></tr>"
//                + "<tr><td> <br /> </td></tr>"
//                + "<tr><td>Please note that the investment will be made as soon as {{LabelShortName}} receives confirmation that the bank has successfully debited your account </td></tr>"
//                + "(One business day after the DD date)"
//                + "(Two business days after the ECS date)"
//                + "<tr><td>If you have any queries in this regard, please contact us at {{contact}}. we&#39;ll be happy to assist you.</td></tr>"
//                + "<tr><td> <br /> </td></tr>"
//                + "<tr><td>Regards,</td></tr>"
//                + "<tr><td>FI.</td></tr>"
//                + "<tr><td> <br /> </td></tr>"
//                + "</table>"
//                + "</span>"
//                + "</body></html>");
//        ctr.setTemplate(template);
//        client.createTemplate(ctr);
        
        try {

            emailList = sesBOObj.getEmailList();
            for (SESBO email : emailList) {
                JSONObject replacementTempData = new JSONObject();
                replacementTempData.put("Investor_Name", email.getInvestorName());
                replacementTempData.put("LabelShortName", "FundsIndia");
                replacementTempData.put("contact", "044-FUNDSINDIA");
                replacementTempData.put("TransType", email.getTransType());
                replacementTempData.put("SchemeName",email.getSchemeName());
                replacementTempData.put("InvestorAmount",email.getInvestorAmount());
                replacementTempData.put("ECS_TOT_AMT",email.getECS_TOT_AMT());
                replacementTempData.put("ECS_DR_DATE",email.getECS_DR_DATE());
                replacementTempData.put("BankName",email.getBankName());
                replacementTempData.put("AccountNumber",email.getAccountNumber());
                BulkEmailDestination destinations = new BulkEmailDestination();
                Destination destination = new Destination();
                List<String> destinationList = new ArrayList<>(1);
                destinationList.add(email.getEmailid());
                destination.setToAddresses(destinationList);
                destinations.setDestination(destination);
                destinations.setReplacementTemplateData(replacementTempData.toString());
                bulkDestinations.add(destinations);
            }
            defaultReplacementTempData.put("InvestName", "Friend");
            defaultReplacementTempData.put("LabelShortName", "FundsIndia");
            defaultReplacementTempData.put("contact", "044-FUNDSINDIA");
            defaultReplacementTempData.put("TransType", "DUMMY");
            defaultReplacementTempData.put("SchemeName","DUMMY");
            defaultReplacementTempData.put("InvestorAmount","DUMMY");
            defaultReplacementTempData.put("ECS_TOT_AMT","DUMMY");
            defaultReplacementTempData.put("ECS_DR_DATE","DUMMY");
            defaultReplacementTempData.put("BankName","DUMMY");
            defaultReplacementTempData.put("AccountNumber","DUMMY");
            sbter.setSource("vigneshanand.others@gmail.com");
            sbter.setTemplate("TPSL_Sample_SIP_Template2");
            sbter.setDestinations(bulkDestinations);
            sbter.setDefaultTemplateData(defaultReplacementTempData.toString());
            client.sendBulkTemplatedEmail(sbter);
            client.shutdown();
        } catch (Exception ex) {
            logger.error("Error while sending the mail :" + ex);
        }
    }
}

//                // Create a message with the specified information. 
//                MimeMessage msg = new MimeMessage(session);
//                msg.setFrom(new InternetAddress(properties.getProperty("FROM"), "FundsIndia"));
//                msg.setRecipient(Message.RecipientType.TO, new InternetAddress(email.getEmailid()));
//                msg.setSubject(sendMailproperties.getProperty("A.E.M.Subject"));
//                StringBuffer mailContent = new StringBuffer();
//                mailContent.append("<span style='font-size:12px;'>");
//                mailContent.append("<table border=0 cellspacing=0 cellpadding=0>");
//                mailContent.append("<tr><td>" + sendMailproperties.getProperty("A.E.M.Greeting").replace("<<!Investor_Name>>", email.getInvestorName()) + "</td></tr>");
//                mailContent.append("<tr><td> <br /> </td></tr>");
//                mailContent.append("<tr><td>" + sendMailproperties.getProperty("A.E.M.Content") + "</td></tr>");
//                mailContent.append("<tr><td> <br /> </td></tr>");
//                mailContent.append("<tr><td>" + sendMailproperties.getProperty("A.E.M.Ignore") + "</td></tr>");
//                mailContent.append("<tr><td> <br /> </td></tr>");
//                String mContent = mailContent.toString();
//                msg.setContent(mContent, "text/html; charset=utf-8");
//
//                // Create a transport.        
//                Transport transport = session.getTransport();
//
//                // Send the message.
//                try {
//                   logger.info("Attempting to send an email through the Amazon SES SMTP interface...");
//                    Stopwatch timer = Stopwatch.createStarted();
//                    logger.info("Host : " + properties.getProperty("HOST") + "Username : " + properties.getProperty("SMTP_USERNAME") +"Password : " + properties.getProperty("SMTP_PASSWORD"));
//                    // Connect to Amazon SES using the SMTP username and password you specified above.
//                    transport.connect(properties.getProperty("HOST"), properties.getProperty("SMTP_USERNAME"), properties.getProperty("SMTP_PASSWORD"));
//                    logger.info("Sending mail to :"+email.getEmailid());
//                    // Send the email.
//                    transport.sendMessage(msg, msg.getAllRecipients());
//                    logger.info(" Time taken in SES " + timer.stop());
//                    logger.info("Email sent!");
//                } catch (Exception ex) {
//                    logger.error("The email was not sent.");
//                    logger.error("Error message: " + ex.getMessage());
//                } finally {
//                    // Close and terminate the connection.
//                    transport.close();
//                }
//            }     

//            GetTemplateRequest gtr = new GetTemplateRequest();
//            gtr.setTemplateName("Sample_HTML_Mail");
//            client.getTemplate(gtr);

