package com.wifs.utils;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import org.apache.log4j.Logger;

public class DBConnection {
	public static Logger logger = Logger.getLogger("DBConnection.class");
	
	public  Connection getWifsSQLConnect() throws Exception {
		Connection objConnection = null;		
		String SQLURL		=	"";
		String SQLDriver	=	"";
		String SQLUserName	=	"";
		String SQLPassword	=	"";
		Properties properties = new Properties();
		
		try{
			// For Database Connection from Properties files
			File fileDir = new File (".");
			String strDirPath	=	 fileDir.getCanonicalPath();
                        logger.info(strDirPath);
			//logger.debug(" Property File Directory : "+strDirPath);
			FileInputStream PropertyFile = new FileInputStream(strDirPath+File.separator+"properties"+File.separator+"config.properties");
			properties.load(PropertyFile);
			
			SQLURL		=	properties.getProperty("SQLURL");
			SQLDriver	=	properties.getProperty("SQLDriverName");
			SQLUserName	=	properties.getProperty("SQLUserName");
			SQLPassword	=	properties.getProperty("SQLPassword");
			Class.forName(SQLDriver);
			objConnection = DriverManager.getConnection(SQLURL,SQLUserName,SQLPassword);
			
		}catch(Exception e){
			logger.error("Exception while getting connection", e);
			e.printStackTrace();
			
			try{
				objConnection = DriverManager.getConnection(SQLURL,SQLUserName,SQLPassword);
				logger.debug("catch con success");
			}catch(Exception e1){
				logger.error("Exception while getting connection", e1);
				e1.printStackTrace();
			}
		}			
		return objConnection;
	}
}
