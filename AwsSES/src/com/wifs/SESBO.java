/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wifs;

import java.util.Date;

/**
 *
 * @author vignesh
 */
public class SESBO {
    
    private String emailid;
    private String investorName;
    private String TransType;
    private String SchemeName;
    private int InvestorAmount;
    private int ECS_TOT_AMT;
    private Date ECS_DR_DATE;
    private String BankName;
    private String AccountNumber;

    /**
     * @return the emailid
     */
    public String getEmailid() {
        return emailid;
    }

    /**
     * @param emailid the emailid to set
     */
    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    /**
     * @return the investorName
     */
    public String getInvestorName() {
        return investorName;
    }

    /**
     * @param investorName the investorName to set
     */
    public void setInvestorName(String investorName) {
        this.investorName = investorName;
    }

    /**
     * @return the TransType
     */
    public String getTransType() {
        return TransType;
    }

    /**
     * @param TransType the TransType to set
     */
    public void setTransType(String TransType) {
        this.TransType = TransType;
    }

    /**
     * @return the SchemeName
     */
    public String getSchemeName() {
        return SchemeName;
    }

    /**
     * @param SchemeName the SchemeName to set
     */
    public void setSchemeName(String SchemeName) {
        this.SchemeName = SchemeName;
    }

    /**
     * @return the InvestorAmount
     */
    public int getInvestorAmount() {
        return InvestorAmount;
    }

    /**
     * @param InvestorAmount the InvestorAmount to set
     */
    public void setInvestorAmount(int InvestorAmount) {
        this.InvestorAmount = InvestorAmount;
    }

    /**
     * @return the ECS_TOT_AMT
     */
    public int getECS_TOT_AMT() {
        return ECS_TOT_AMT;
    }

    /**
     * @param ECS_TOT_AMT the ECS_TOT_AMT to set
     */
    public void setECS_TOT_AMT(int ECS_TOT_AMT) {
        this.ECS_TOT_AMT = ECS_TOT_AMT;
    }

    /**
     * @return the ECS_DR_DATE
     */
    public Date getECS_DR_DATE() {
        return ECS_DR_DATE;
    }

    /**
     * @param ECS_DR_DATE the ECS_DR_DATE to set
     */
    public void setECS_DR_DATE(Date ECS_DR_DATE) {
        this.ECS_DR_DATE = ECS_DR_DATE;
    }

    /**
     * @return the BankName
     */
    public String getBankName() {
        return BankName;
    }

    /**
     * @param BankName the BankName to set
     */
    public void setBankName(String BankName) {
        this.BankName = BankName;
    }

    /**
     * @return the AccountNumber
     */
    public String getAccountNumber() {
        return AccountNumber;
    }

    /**
     * @param AccountNumber the AccountNumber to set
     */
    public void setAccountNumber(String AccountNumber) {
        this.AccountNumber = AccountNumber;
    }
    
    
    
    
}
